# Stage 1
FROM microsoft/dotnet AS builder
WORKDIR /source

# caches restore result by copying csproj file separately
COPY /DiaryService/Api/Api.csproj ./src/Api/
WORKDIR /source/src/Api
RUN dotnet restore 

# copies the rest of your code
WORKDIR /source/
COPY DiaryService/Api/. ./src/Api/
WORKDIR /source/src/Api
RUN dotnet publish -c Release -o out

# Stage 2
FROM microsoft/aspnetcore
WORKDIR /app
COPY --from=builder /source/src/Api/out ./
EXPOSE 5000/tcp
ENTRYPOINT ["dotnet", "Api.dll"]