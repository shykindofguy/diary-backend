﻿using System.Collections.Generic;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    public class HomeController 
    {
        [Route("")]
        public string IsAlive()
        {
            var version = typeof(HomeController).GetTypeInfo()
                .Assembly
                .GetCustomAttribute<AssemblyInformationalVersionAttribute>()
                .InformationalVersion;

            return version;
        }
    }
}