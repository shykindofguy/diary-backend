using System;
using Xunit;

namespace SomeTests
{
    public class UnitTest1
    {
        [Fact]
        public void SomethingIsNotNull()
        {
            var something = new object();
            
            Assert.NotNull(something);
        }
        
        [Fact]
        public void SomethingIsNull()
        {
            object something = null;
            
            Assert.Null(something);
        }
    }
}