# Diary Backend

Task board: https://trello.com/b/qfkxBEJJ/diary-taskboard 

To build docker image - docker build -t diary-api . 
To run Diary service - docker run -it --rm -p 5000:80 --name my-diary-api-example diary-api
To tag version - docker tag diary-api serjebulavsky/diary-api